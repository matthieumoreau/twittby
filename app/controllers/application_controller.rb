class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation, :remember_me) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :username, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:avatar, :first_name, :last_name, :place, :birthday_date, :username, :email, :password, :description, :password_confirmation, :current_password) }
  end

  helper do 
    def link_hashtag(url,text=url,opts={})
      attributes = ""
      opts.each { |key,value| attributes << key.to_s << "=\"" << value << "\" "}
      link_to("#{attributes}#{text}", "#{url}")

    end

    def content_hashtag_link(content)
      if ("#{content}".scan(/#\w+/)).empty? == false
        ("#{content}".gsub!(/#\w+/) do |tag| diese ="#{tag}".tr('#', ''); 
        link_hashtag("#{hashtags_url}/#{diese}", "##{diese}") end).html_safe
      else
        "#{content}"
      end
    end
  end 
end
