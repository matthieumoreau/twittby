class FavoriteTweetsController < ApplicationController
  before_action :set_tweet
  
  def create
    if Favorite.create(favorited: @tweet, user: current_user)
      redirect_to tweets_path, notice: 'Tweet has been favorited'
    else
      redirect_to tweets_path, alert: 'Something went wrong...*sad panda*'
    end
  end
  
  def destroy
    Favorite.where(favorited_id: @tweet.id, user_id: current_user.id).first.destroy
    redirect_to tweets_path, notice: 'Tweet is no longer in favorites'
  end
  
  private
  
  def set_tweet
    @tweet = Tweet.find(params[:tweet_id] || params[:id])
    return @tweet
  end
end