class TweetsController < ApplicationController

  def index
    if user_signed_in?
      @tweets = Tweet.feed(current_user.id)
      @user = User.find(current_user.id)
    else 
       @tweets = Tweet.all
    end
  end

  def new
    @new_tweet = Tweet.new
    @user = User.find(current_user.id)
  end

def show
    @tweet = Tweet.find(params[:id])
    if user_signed_in?
    @user = User.find(current_user.id)
    end
  end

  def create
    if (params[:tweet][:place_id] == "" || params[:tweet][:place_id] == "") 
       flash[:notice] = "Veuillez sélectionner un genre de musique et/ou un lieux"
       redirect_to(tweets_new_path)
    else
      tweet = Tweet.create(
        user_id: current_user.id,
        content: params[:tweet][:content], 
        place_id: params[:tweet][:place_id], 
        music_id: params[:tweet][:music_id]
      )
       redirect_to(tweets_path)
    end
  end
end