class SearchesController < ApplicationController

	def search_mention
		mentions = User.where("username LIKE ?", "%#{params[:q]}%")
    	render json: mentions
	end

	def search_hashtag
		hashtags = SimpleHashtagHashtag.where("name LIKE ?", "%#{params[:q]}%")
		render json: hashtags
	end

	def search_music
		musics = Music.where("name LIKE ?", "%#{params[:q]}%")
		render json: musics
	end

	def search_place
		places = Place.where("name LIKE ?", "%#{params[:q]}%")
		render json: places
	end

	def show_result
		@music = Music.search(params[:search_music])
		@place = Place.search(params[:search_place])

		@tweets_result = Tweet.search(@music.id, @place.id)
		@user = User.find(current_user.id)

		if @tweets_result.count == 0
			flash[:notice] = "Il n'y aucun résultat correspondant à votre recherche"
		end
	end

end
