class LandingController < ApplicationController
	def show
		if current_user 
			redirect_to tweets_path
		end
	end	
end
