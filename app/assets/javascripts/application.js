// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require turbolinks
//= require bootstrap
//= require handlebars
//= require tweets
//= require_tree .

$(document).ready(function() {

	/**********************************
	MUSICS SEARCH
	**********************************/	

	var musics = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  //prefetch: 'searches/musics.json',
  	  remote: '/searches/musics.json?q=%QUERY'
	});
	 
	musics.initialize();
	 
	$('.input-music').typeahead(null, {
	  name: 'musics-search',
	  displayKey: 'name',
	  source: musics.ttAdapter(),
	  templates: {
	    empty: [
	      '<div class="empty-message">',
	      'no result',
	      '</div>'
	    ].join('\n'),
	    suggestion: Handlebars.compile('<p>{{name}}</p>')
	  }
	});

	/**********************************
	PLACES SEARCH
	**********************************/	

	var places = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  //prefetch: 'searches/places.json',
	  remote: '/searches/places.json?q=%QUERY'
	});
	 
	places.initialize();
	 
	$('.input-place').typeahead(null, {
	  name: 'places-search',
	  displayKey: 'name',
	  source: places.ttAdapter(),
	  templates: {
	    empty: [
	      '<div class="empty-message">',
	      'no result',
	      '</div>'
	    ].join('\n'),
	    suggestion: Handlebars.compile('<p>{{name}}</p>')
	  }
	});

	/**********************************
	HASHTAGS/MENTIONS SEARCH
	**********************************/	

	var hashtags = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  //prefetch: 'searches/hashtags.json',
	  remote: '/searches/hashtags.json?q=%QUERY'
	});
	 
	var mentions = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('username'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  //prefetch: 'searches/mentions.json',
	  remote: '/searches/mentions.json?q=%QUERY'
	});
	 
	hashtags.initialize();
	mentions.initialize();
	 
	$('.input-hashtags-mentions').typeahead({
	  highlight: true
	},
	{
	  name: 'hashtags-search',
	  displayKey: 'name',
	  source: hashtags.ttAdapter(),
	  templates: {
	    header: '<h3 class="league-name">Hashtags</h3>',
	    empty: [
	      '<div class="empty-message">',
	      'no result',
	      '</div>'
	    ].join('\n'),
	    suggestion: Handlebars.compile('<a href="/hashtags/{{name}}">#{{name}}</a>')
	  }
	},
	{
	  name: 'mentions-search',
	  displayKey: 'username',
	  source: mentions.ttAdapter(),
	  templates: {
	    header: '<h3 class="league-name">Mentions</h3>',
	    empty: [
	      '<div class="empty-message">',
	      'no result',
	      '</div>'
	    ].join('\n'),
	    suggestion: Handlebars.compile('<a href="/users/{{id}}">@{{username}}</a>')
	  }
	});

	/**********************************
	FAVORITES
	**********************************/	

	$(".favorite").click(function() {
  	  var tweet_id = $(this).attr('id');
	  $.ajax({
	    type: "POST",
	    url: 'favorites/' + tweet_id,
	    success: function() {
	      // change image or something
	    }
	  })
	});

});