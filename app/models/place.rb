class Place < ActiveRecord::Base
	has_many :tweet

	def self.search(query)
	  where("name like ?", "%#{query}%").first_or_create
	end
end
