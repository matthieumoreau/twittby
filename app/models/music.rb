class Music < ActiveRecord::Base
	has_many :tweet

	# Search by music
	def self.search(query)
		where("name like ?", "%#{query}%").first_or_create
	end
end
