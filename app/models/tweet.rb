class Tweet < ActiveRecord::Base
	include SimpleHashtag::Hashtaggable
	hashtaggable_attribute :content

	has_many :favorites, as: :favorited

	belongs_to :place
	belongs_to :music
	belongs_to :user

	validates :content,  presence: true, :length => { :maximum => 140}
	validates :user_id,  presence: true

	default_scope { order('tweets.created_at DESC')}


	def self.search(music, place)
		m = "%#{music}%"
		p = "%#{place}%"
		where("music_id like ? AND place_id like ?", m, p).order(created_at: :desc)
	end

	def self.feed(current_user)
		first = Tweet.where(user_id: Relationship.select(:followed_id).where(follower_id: "#{current_user}")).order(created_at: :asc)
		second = Tweet.where(user_id: "#{current_user}").order(created_at: :asc)
		return (first + second).sort{|a,b| a.created_at <=> b.created_at }.reverse

	end 

	def self.feedByUser(user_id)
		Tweet.where(user_id: "#{user_id}").order(created_at: :desc)
	end 


end