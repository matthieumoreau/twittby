class CreateTweets < ActiveRecord::Migration
  def change
    create_table :tweets do |t|
      t.string :content
      t.string :media
      t.integer :user_id
      t.integer :music_id
      t.integer :place_id
      t.datetime :created_at

      t.timestamps
    end
  end
end
