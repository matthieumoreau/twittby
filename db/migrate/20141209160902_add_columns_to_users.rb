class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :place, :string
    add_column :users, :description, :string
    add_column :users, :birthday_date, :datetime
  end
end
