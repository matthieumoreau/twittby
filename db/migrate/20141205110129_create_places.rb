class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :name
      t.datetime :created_at

      t.timestamps
    end
  end
end
