# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Following relationships


User.create(first_name: "Jean", last_name: "Sérien", username: "jean.serien")
User.create(first_name: "Paul", last_name: "Aumenton", username: "paul.aumenton")
User.create(first_name: "Marie", last_name: "Toua", username: "marie.toua")
User.create(first_name: "Robin", last_name: "Débois", username: "robin.debois")
User.create(first_name: "Marc", last_name: "Opolo", username: "marc.opolo")



Music.create([
	{ name: 'Electronique' }, 
	{ name: 'Dubstep' },
	{ name: 'Classique' },
	{ name: 'Rock' }, 
	{ name: 'Pop' },
	{ name: 'Hip-hop' },
	{ name: 'Alternative' }, 
	{ name: 'House' },
	{ name: 'Jazz' }
])

Place.create([
	{ name: 'Le Showcase' }, 
	{ name: 'Le Concrete' },
	{ name: 'Olympia' },
	{ name: 'Zenith de Paris' }, 
	{ name: 'La Cigale' },
	{ name: 'La Maroquinerie' },
	{ name: 'Cabaret Sauvage' }, 
	{ name: 'Bataclan' },
	{ name: 'Palais des sports' }
])


